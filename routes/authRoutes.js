const { Router } = require('express');
const { UserService } = require('../services/userService');
const { asyncWrapper } = require('../util/asyncWrapper');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const emailAndPassValidation = require('../joiValidationSchema/userCreationSchema');

const router = Router();

const userService = UserService.getInstance();

router.post('/register', emailAndPassValidation, asyncWrapper(async function(req, res) {
    const {email, password, role} = req.body;
    const user = await userService.create(email, password, role);

    return res.status(200).json({message: 'Profile created successfully'});
}));

router.post('/login', emailAndPassValidation, asyncWrapper(async function(req, res) {
    const {email, password } = req.body;
    const user = await userService.login(email, password);
    const token = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);

    return res.status(200).json({ jwt_token: token});

}));

router.post('/forgot_password', asyncWrapper(async function(req, res) {
    const { email } = req.body;
    const user = await userService.getNewPass(email);

    return res.status(200).json({message: 'New password sent to your email address'});

}));

module.exports = router;