const express = require('express');
const { TruckService } = require('../services/truckServices');
const { asyncWrapper } = require('../util/asyncWrapper');

const router = express.Router();

const truckService = TruckService.getInstance();

router.get('/', asyncWrapper(async function(req, res) {
    const trucks = await truckService.get(req.user._id);
    
    return res.status(200).json({trucks: trucks});
}));

router.post('/', asyncWrapper(async function(req, res) {
    const id = req.user._id;
    const { type } = req.body;
    const truck = await truckService.add(id, type);
    return res.status(200).json({message: 'Truck created successfully'});
}));

router.get('/:id', asyncWrapper(async function(req, res) {
    const truckId = req.params.id;
    const id = req.user._id;

    const truck = await truckService.getById(truckId, id);
    
    return res.status(200).json({truck: truck});
}));

router.put('/:id', asyncWrapper(async function(req, res) {
    const truckId = req.params.id;
    const id = req.user._id;
    const { type } = req.body;

    const truck = await truckService.update(type, truckId, id);
    
    return res.status(200).json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async function(req, res) {
    const truckId = req.params.id;
    const id = req.user._id;

    const truck = await truckService.delete( truckId, id);
    
    return res.status(200).json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async function(req, res) {
    const truckId = req.params.id;
    const id = req.user._id;

    const truck = await truckService.assign(truckId, id);
    
    return res.status(200).json({message: 'Truck assigned successfully'});
}));




module.exports = router;
