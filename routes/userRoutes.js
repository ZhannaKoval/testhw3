const express = require('express');
const { UserService } = require('../services/userService');
const { UserDTO } = require('../dto/user.dto');
const { asyncWrapper } = require('../util/asyncWrapper');
const passwordValid = require('../joiValidationSchema/passwordValidationSchema');

const router = express.Router();

const userService = UserService.getInstance();


router.get('/me', asyncWrapper(async function(req, res) {
    const user = await userService.get(req.user._id);
    return res.status(200).json({user: new UserDTO(user)});
}));

router.delete('/me', asyncWrapper(async function(req, res) {
    const user = await userService.delete(req.user._id);
    return res.status(200).json({message: 'Profile deleted successfully'});
}));

router.patch('/me/password', passwordValid, asyncWrapper(async function(req, res) {
  const {oldPassword, newPassword} = req.body;
  const user = await userService.get(req.user._id, oldPassword, newPassword);
    return res.status(200).json({message: 'Password changed successfully'});
}));

module.exports = router;