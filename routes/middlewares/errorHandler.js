const errorHandler = (err, req, res, next) => {
    return res.status(err.status || 400).json({ message: err.message });
};
  
module.exports = { errorHandler };