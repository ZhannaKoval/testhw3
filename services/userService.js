const { User } = require('../models/User');
const bcrypt = require('bcrypt');

class UserService {

   async create(email, password, role) {
        const hashedPass = await bcrypt.hash(password, 10);
    
        const user = new User({
            email,
            password: hashedPass,
            role
          });
        
        await user.save();
        
        return user;
    }

    async login(email, password) {
        const user = await User.findOne({email});

        if (!user) {
            throw new Error (`Wrong email!`);
        }

        const compareRes = await bcrypt.compare(password, user.password);


        if (!compareRes) {
            throw new Error (`Wrong password!`);
        }
        
        return user;
    }

    get(id) {
        return User.findOne({ _id: id });
    }

    delete(id) {
        return User.deleteOne({ _id: id });
    }

    async update(id, { oldPassword, newPassword }) {
        
            const user = await User.findOne({_id: id});

            const passwordsMatch = await bcrypt.compare(newPassword, user.password);

            if (passwordsMatch) {
                throw new Error (`Wrong old password!`);
            }
        
            if(oldPassword.split("").sort().join("") === newPassword.split("").sort().join("")) {
                throw new Error (`Passwords are the same!`);
            }
        
            user.password = await bcrypt.hash(newPassword, 10);;
            return user.save();
    }

    async getNewPass(email) {
        const user = await User.findOne({email});

        if (!user) {
            throw new Error (`Wrong email!`);
        }

        return user;

    }
}

UserService.getInstance = () => {
    if (!UserService.instance) {
        UserService.instance = new UserService();
    }

    return UserService.instance;
};

module.exports = { UserService };